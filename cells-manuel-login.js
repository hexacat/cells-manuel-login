{
  const {
    html,
  } = Polymer;
  /**
    `<cells-manuel-login>` Description.

    Example:

    ```html
    <cells-manuel-login></cells-manuel-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-manuel-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsManuelLogin extends Polymer.Element {

    static get is() {
      return 'cells-manuel-login';
    }

    static get properties() {
      return {
        name: {
          type: String,
          value: ""
        },
        password: {
          type: String,
          value: ""
        },
        logged: {
          type: Boolean,
          value:false,
          notify: true
        }
      };
    }

    validatelogin() {
      console.log("Aqui esta el evento prueba");
      if(this.name == "Manuel" && this.password == "1234") {
        this.set("logged", true);
        this.dispatchEvent(new CustomEvent('login-success',{detail:this.name}));
        console.log("Acceso correcto");
      } else {
        this.dispatchEvent(new CustomEvent('login-error',{detail:this.name}));
        console.log("Acceso incorrecto");
      }
    }



    static get template() {
      return html `
      <style include="cells-manuel-login-styles cells-manuel-login-shared-styles"></style>
      <slot></slot>

        <input value="{{name::input}}" id="idname" type="text" placeholder="username" required/>
        <input value="{{password::input}}" id="idpassword" type="password" placeholder="password" required/>
        <button type="button" on-click="validatelogin">login</button>

      `;
    }
  }

  customElements.define(CellsManuelLogin.is, CellsManuelLogin);
}
